<?php

/**
 * @file
 * Usercentrics CMP module file.
 */

/**
 * Implements hook_library_info_build().
 *
 * Adds the Usercentrics CMP library.
 */
function usercentrics_cmp_library_info_build() {
  $config = \Drupal::config('usercentrics_cmp.settings');
  $settings_id = $config->get('settings_id');
  $enable_tcf = $config->get('enable_tcf');
  $enable_sdp = $config->get('enable_sdp');

  $libraries = [];

  if ($enable_sdp) {
    $libraries['sdp'] = [
      'version' => '1.x',
      'header' => TRUE,
      'js' => [
        'https://privacy-proxy.usercentrics.eu/latest/uc-block.bundle.js' => [
          'type' => 'external',
          'minified' => TRUE,
        ],
      ],
    ];
  }

  $libraries['usercentrics_cmp'] = [
    'version' => '2.x',
    'header' => TRUE,
    'js' => [
      'https://app.usercentrics.eu/browser-ui/latest/loader.js' => [
        'type' => 'external',
        'minified' => TRUE,
        'attributes' => [
          'data-settings-id' => $settings_id,
          'data-tcf-enabled' => $enable_tcf ? TRUE : FALSE,
          'id' => "usercentrics-cmp",
          'defer' => TRUE,
          'async' => TRUE,
        ],
      ],
    ],
  ];

  return $libraries;
}

/**
 * Implements hook_library_info_alter().
 *
 * Adds the Usercentrics CMP behavior to libraries
 * that belong to an enabled data processing service.
 */
function usercentrics_cmp_library_info_alter(&$libraries, $extension) {
  $enabled_services = usercentrics_cmp_get_enabled_services();

  foreach ($libraries as $library_name => $library) {
    foreach ($enabled_services as $data_processing_service) {
      if (!isset($data_processing_service['libraries'])) {
        continue;
      }
      foreach ($data_processing_service['libraries'] as $library_key) {
        if ($library_key === implode('.', [$extension, $library_name])) {
          foreach ($library['js'] as $js_key => $js) {
            $libraries[$library_name]['js'][$js_key]['attributes']['type'] = 'text/plain';
            $libraries[$library_name]['js'][$js_key]['attributes']['data-usercentrics'] = $data_processing_service['name'];
          }
        }
      }
    }
  }
}

/**
 * Implements hook_page_attachments().
 *
 * Inserts JavaScript for the Usercentrics CMP to the page.
 */
function usercentrics_cmp_page_attachments(array &$attachments) {
  if (\Drupal::service('router.admin_context')->isAdminRoute()) {
    return;
  }

  if (\Drupal::config('usercentrics_cmp.settings')->get('enable_sdp')) {
    $attachments['#attached']['library'][] = 'usercentrics_cmp/sdp';
  }

  $attachments['#attached']['library'][] = 'usercentrics_cmp/usercentrics_cmp';
}

/**
 * Implements hook_page_attachments_alter().
 *
 * Alters the JavaScript tags to comply with the consent manager.
 */
function usercentrics_cmp_page_attachments_alter(array &$attachments) {
  if (\Drupal::service('router.admin_context')->isAdminRoute()) {
    return;
  }

  $link_attachments = usercentrics_cmp_get_link_attachments();
  foreach ($link_attachments as $link_attachment_id => $link_attachment_attributes) {
    $attachments['#attached']['html_head'][] = [
      [
        '#tag' => 'link',
        '#attributes' => $link_attachment_attributes,
      ],
      $link_attachment_id,
    ];
  }

  $enabled_services = usercentrics_cmp_get_enabled_services();
  foreach ($enabled_services as $data_processing_service) {
    foreach ($data_processing_service['attachments'] as $attachment_key) {
      foreach ($attachments['#attached']['html_head'] as $index => $attachment) {
        if (!isset($attachment[1])) {
          continue;
        }
        $match = (@preg_match($attachment_key, '') !== FALSE) ? preg_match($attachment_key, $attachment[1]) : $attachment_key === $attachment[1];
        if ($match) {
          $attachments['#attached']['html_head'][$index][0]['#attributes']['type'] = 'text/plain';
          $attachments['#attached']['html_head'][$index][0]['#attributes']['data-usercentrics'] = $data_processing_service['name'];
        }
      }
    }
  }
}

/**
 * Returns the link attachments for the usercentric scripts.
 */
function usercentrics_cmp_get_link_attachments() {
  $link_attachments = [
    'usercentrics_cmp_preconnect_app' => [
      'rel' => 'preconnect',
      'href' => '//app.usercentrics.eu',
    ],
    'usercentrics_cmp_preconnect_api' => [
      'rel' => 'preconnect',
      'href' => '//app.usercentrics.eu',
    ],
    'usercentrics_cmp_preload_app' => [
      'rel' => 'preload',
      'href' => '//app.usercentrics.eu/browser-ui/latest/loader.js',
    ],
  ];

  if (\Drupal::config('usercentrics_cmp.settings')->get('enable_sdp')) {
    $link_attachments += [
      'usercentrics_cmp_preconnect_privacy_proxy' => [
        'rel' => 'preconnect',
        'href' => '//privacy-proxy.usercentrics.eu',
      ],
      'usercentrics_cmp_preload_privacy_proxy' => [
        'rel' => 'preload',
        'href' => '//privacy-proxy.usercentrics.eu/latest/uc-block.bundle.js',
      ],
    ];
  }

  return $link_attachments;
}

/**
 * Returns the enabled data processing services.
 */
function usercentrics_cmp_get_enabled_services() {
  $config = \Drupal::config('usercentrics_cmp.settings');
  $services = $config->get('data_processing_services');

  $data_processing_services = usercentrics_cmp_data_processing_services();
  $enabled_services = [];
  foreach ($data_processing_services as $data_processing_service) {
    if (isset($services[$data_processing_service['id']])) {
      $enabled_services[] = $data_processing_service;
    }
  }

  return $enabled_services;
}

/**
 * Provides a list of all available data processing services.
 *
 * Other modules may modify the list using the hook:
 * hook_usercentrics_cmp_data_processing_services_alter()
 */
function usercentrics_cmp_data_processing_services() {
  $data_processing_services = [
    [
      'id' => 'ko1w5PpFl',
      'name' => 'Facebook Pixel',
      'attachments' => ['facebook_tracking_pixel_script'],
    ],
    [
      'id' => 'HkocEodjb7',
      'name' => 'Google Analytics',
      'libraries' => ['ga.analytics'],
      'attachments' => [
        'google_analytics_tracking_script',
        'google_analytics_tracking_file',
      ],
    ],
    [
      'id' => 'BJ59EidsWQ',
      'name' => 'Google Tag Manager',
      'attachments' => ['/^google_tag_script_tag/'],
    ],
    [
      'id' => 'zqWojrT0P',
      'name' => 'Matomo',
      'attachments' => ['matomo_tracking_script'],
    ],
    [
      'id' => 'u6fxocwTs',
      'name' => 'Matomo (self hosted)',
      'attachments' => ['matomo_tracking_script'],
    ],
  ];

  \Drupal::moduleHandler()->alter('usercentrics_cmp_data_processing_services', $data_processing_services);

  return $data_processing_services;
}
