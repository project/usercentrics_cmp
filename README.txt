Module: Usercentrics Consent Management Platform (CMP)
Author: David Fröhlich


Description
===========
Adds the Usercentrics Consent Management Platform to your website.

Supported services:

* Facebook Pixel (facebook_tracking_pixel)
* Google Analytics (google_analytics), Googalytics (ga)
* Google Tag Manager (google_tag)
* Matomo (matomo)


Requirements
============

* Usercentrics account


Installation
============
Copy the 'usercentrics_cmp' module directory in to your Drupal 'modules'
directory as usual.


Usage
=====
In the settings page enter your Usercentrics Settings-ID, enable the
relevant data processing services and clear the cache.
