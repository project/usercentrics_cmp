<?php

namespace Drupal\usercentrics_cmp\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides settings for usercentrics_cmp module.
 */
class UsercentricsCmpConfigForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'usercentrics_cmp.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'usercentrics_cmp_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $form['settings_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Settings-ID'),
      '#default_value' => $config->get('settings_id'),
    ];

    $form['enable_tcf'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable TCF'),
      '#default_value' => $config->get('enable_tcf'),
    ];

    $form['enable_sdp'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Smart Data Protector'),
      '#default_value' => $config->get('enable_sdp'),
    ];

    $data_processing_services_options = [];
    $data_processing_services = usercentrics_cmp_data_processing_services();
    foreach ($data_processing_services as $data_processing_service) {
      $data_processing_services_options[$data_processing_service['id']] = $data_processing_service['name'];
    }

    $form['data_processing_services'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Data Processing Services'),
      '#default_value' => $config->get('data_processing_services'),
      '#options' => $data_processing_services_options,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $data_processing_services = $form_state->getValue('data_processing_services');
    foreach ($data_processing_services as $index => $data_processing_service) {
      if (!$data_processing_service) {
        unset($data_processing_services[$index]);
      }
    }

    $this->configFactory->getEditable(static::SETTINGS)
      ->set('settings_id', $form_state->getValue('settings_id'))
      ->set('enable_tcf', $form_state->getValue('enable_tcf'))
      ->set('enable_sdp', $form_state->getValue('enable_sdp'))
      ->set('data_processing_services', $data_processing_services)
      ->save();

    parent::submitForm($form, $form_state);
  }

}
