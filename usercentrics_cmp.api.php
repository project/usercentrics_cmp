<?php

/**
 * @file
 * Hooks specific to the module.
 */

/**
 * Allows to alter the list of available data processing services.
 *
 * Each data processing service item should be an array with these keys:
 * - id = the unique id of the service.
 * - name = the name that should be used for the 'data-usercentrics' attribute.
 * - libraries = a list of all libraries that belong to the service.
 * - attachments = a list of all attachments that belong to the service.
 *
 * @param array $data_processing_services
 *   The array of available data processing services.
 */
function hook_usercentrics_cmp_data_processing_services_alter(array &$data_processing_services) {
  $data_processing_services[] = [
    'id' => 'MY_ID',
    'name' => 'MY_NAME',
    'libraries' => ['MY_MODULE.MY_LIBRARY'],
    'attachments' => ['MY_ATTACHMENT_KEY'],
  ];
}
